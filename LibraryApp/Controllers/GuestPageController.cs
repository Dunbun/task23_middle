﻿using LibraryApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class GuestPageController : Controller
    {
        List<Report> reportsList = new List<Report>();
        // GET: GuestPage
        public ActionResult Index()
        {
            Report report = new Report();

            report.AuthorName = "A. Smith";
            report.Date = DateTime.Now.ToString();
            report.ReportText = "Reporort 1 Some text";

            reportsList.Add(report);
            reportsList.Add(report);
            ViewBag.ReportsList = reportsList;

            return View(reportsList);
        }


        [HttpPost]
        public ActionResult AddReport(string authorName, string reportText, List<Report> form )
        {
            Report report = new Report() { AuthorName = authorName, ReportText = reportText, Date = DateTime.Now.ToString()};
            reportsList = form;
            reportsList.Add(report);

            return View("Index", reportsList);
        }


    }
}