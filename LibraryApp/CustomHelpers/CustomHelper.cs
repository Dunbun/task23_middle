﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.CustomHelpers
{
    public static class CustomHelper
    {
        public static MvcHtmlString Image2(this HtmlHelper htmlhelper)
        {
            var questionary = new TagBuilder("form");
            questionary.MergeAttribute("style", "margin:5px");
            questionary.MergeAttribute("action", "/Questionary");
            questionary.MergeAttribute("method", "post");

            var inputText = new TagBuilder("input");
            inputText.MergeAttribute("id", "UserEmail");
            inputText.MergeAttribute("name", "UserEmail");
            inputText.MergeAttribute("placeholder", "Enter your email");
            inputText.MergeAttribute("style", "width: 200px");
            inputText.MergeAttribute("type", "text");
            inputText.MergeAttribute("value", "");

            questionary.InnerHtml += inputText + "<br/>";

            inputText.MergeAttribute("id", "UserName", true);
            inputText.MergeAttribute("name", "UserName", true);
            inputText.MergeAttribute("placeholder", "Enter your name", true);

            questionary.InnerHtml += inputText + "<br/>";
            questionary.InnerHtml += "<span>Skills:</span> <br/> ";

            var divSkills = new TagBuilder("div");
            divSkills.MergeAttribute("style", "display:grid; grid-template-columns:50px 50px; margin-left:40px; width:100px");
            
            var inputCheckBox = new TagBuilder("input");
            inputCheckBox.MergeAttribute("name", "HTML");
            inputCheckBox.MergeAttribute("type", "checkbox");

            divSkills.InnerHtml += inputCheckBox + "<span>HTML</span>";

            inputCheckBox.MergeAttribute("name", "CSS", true);
            divSkills.InnerHtml += inputCheckBox + "<span>CSS</span>";

            inputCheckBox.MergeAttribute("name", "JS", true);
            divSkills.InnerHtml += inputCheckBox + "<span>JS</span>";

            questionary.InnerHtml += divSkills + "<br/> <span>Gender</span> <br/>";

            var ul = new TagBuilder("ul");
            ul.MergeAttribute("style", "margin-left:25px; width: 100px");

            var li = new TagBuilder("li");
            li.InnerHtml = "<input checked='checked' id='Gender' name='Gender' type='radio' value='Male'/><span>Male</span>";

            ul.InnerHtml += li;

            li.InnerHtml = "<input id='Gender' name='Gender' type='radio' value='Female'/><span>Female</span>";
            ul.InnerHtml += li;

            questionary.InnerHtml += ul;

            var submitBtn = new TagBuilder("input");
            submitBtn.MergeAttribute("type", "submit");
            submitBtn.MergeAttribute("name", "submitButton");
            submitBtn.MergeAttribute("value", "Submit");

            questionary.InnerHtml += submitBtn;
            
            return MvcHtmlString.Create(questionary.ToString());
        }
    }
}